#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <graphics_framework.h>
#include "sceneHelper.h"
// Access physics componenets
#include "physics.h"
// Access entity & component structs
#include "game.h"
// Phys utils contains render/fx functions
#include <phys_utils.h>

using namespace std;
using namespace glm;
using namespace graphics_framework;

namespace sh
{
	

	// Setup table object
	unique_ptr<Entity> CreateTable()
	{
		// Create pointer to new Entity
		unique_ptr<Entity> ent(new Entity());
		// Set scale of table
		ent->SetScale(vec3(30.0f, 0.0f, 20.0f));
		// Create render component to define shape
		unique_ptr<cShapeRenderer> renderComp(new cShapeRenderer(cShapeRenderer::BOX));
		// Add created components to entity
		ent->AddComponent(unique_ptr<Component>(move(renderComp)));
		// Add a new sphere collider to entity
		//ent->AddComponent(unique_ptr<Component>(new cPlaneCollider()));
		// Finally retur the full entity
		return ent;
	}

	







	//void RotateCam(double delta_time, vec3 axis)
	//{
	//	// Reset rotation value
	//	float rot = 0.0f;
	//	// Increment rotation value
	//	rot += 0.5f * delta_time;
	//	// Grab current camera position
	//	vec3 camPos = phys::GetCameraPos();
	//	// Update camera position
	//	phys::SetCameraPos(rotate(camPos, rot, axis));
	//}

	// Move cam back/forth whilst facing target
	//void ZoomCam(bool dir)
	//{
	//	// Find cam target pos
	//	vec3 target = phys::GetCameraTarget();
	//	// Find current cam pos
	//	vec3 camPos = phys::GetCameraPos();
	//	// Find facing direction
	//	vec3 lookAt = normalize(target - camPos);
	//	// If true, zoom out
	//	if (dir)
	//		camPos += -lookAt;
	//	// If false, zoom in
	//	else
	//		camPos += lookAt;

	//	// Adjust camera pos
	//	phys::SetCameraPos(camPos);
	//}

	//// Listens for key press; moves cam
	//void CameraControls(double delta_time, target_camera cam)
	//{
	//	// Rotate camera around target
	//	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
	//	{
	//		RotateCam(delta_time, vec3(0.0, 1.0f, 0.0));
	//	}
	//	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
	//	{
	//		RotateCam(delta_time, vec3(0.0, -1.0f, 0.0));
	//	}
	//	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
	//	{
	//		RotateCam(delta_time, vec3(1.0f, 0.0, 0.0));
	//	}
	//	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
	//	{
	//		RotateCam(delta_time, vec3(-1.0f, 0.0, 0.0));
	//	}
	//
	//	// Zoom cam
	//	if (glfwGetMouseButton(renderer::get_window(), GLFW_MOUSE_BUTTON_4))
	//	{
	//		// True = zooming out
	//		ZoomCam(true);
	//		
	//	}
	//	if (glfwGetMouseButton(renderer::get_window(), GLFW_MOUSE_BUTTON_5))
	//	{
	//		// False = zoom in
	//		ZoomCam(false);
	//	}
	//}

	


}