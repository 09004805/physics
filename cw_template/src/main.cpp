#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <graphics_framework.h>
#include <phys_utils.h>
#include "sceneHelper.h"


using namespace std;
using namespace graphics_framework;
using namespace glm;
using namespace sh;
#define physics_tick 1.0 / 60.0

// Balls in scene
struct ball
{
	string name;
	mesh mesh; 
	vec4 colour;
	vec3 curPos;
	vec3 prevPos;
	
	dvec3 forces;
	dvec3 acceleration;
	dvec3 vel = dvec3(0);
	double  coefFriction = -0.5f;


	// Record incoming impulses
	void AddImpulse(dvec3 force)
	{
		forces += force;
	}

};

// Boxes that make up table
struct tablePiece
{
	mesh mesh;
	vec3 pos;
	vec4 colour = vec4(0, 1.0f, 0, 1.0f);
};


// Holds all created balls in scene
map<string, ball> balls;
// Table list made up of table pieces
map<string, tablePiece> table;
// Holds lines representing spacial partitioning grid
map<string, geometry> lines;

// Rendering stuff
target_camera cam;
effect effP;
glm::mat4 PV;
directional_light light;
material mat;
// Keep track of these
vec3 camPos;
vec3 cueballPos;


// Spring properties 
double k = 10;
double t = 0.01;
double m = 1;


// Lines needed to show spacial partitioning grid
geometry gridLineX;
geometry gridLineZ1;
geometry gridLineZ2;
geometry gridLineZ3;
geometry trajectory;
// To allow toggle grid on/off screen
bool showGrid = false;




// Set up camera, light, shader
void Init() {
	// Setup shader
	effP = effect();
	effP.add_shader("shaders/phys_phong.vert", GL_VERTEX_SHADER);
	effP.add_shader("shaders/phys_phong.frag", GL_FRAGMENT_SHADER);
	effP.build();

	// Set cam starting position and target
	cam.set_position(vec3(0.0f, 2.0f, 10.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	// Light properties
	light.set_ambient_intensity(vec4(0.5f, 0.5f, 0.5f, 1.0f));
	light.set_light_colour(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	light.set_direction(vec3(0.0f, 1.0f, 0.0f));
	mat = material(vec4(0.0f, 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), 25.0f);
}

// Create scene objects
void CreateSceneObjs()
{
	// Start with cue ball
	ball cueBall;
	// Set nae
	cueBall.name = "cueBall";
	// Create ball geom
	cueBall.mesh = mesh(geometry_builder::create_sphere(50, 50));
	// Set colour
	cueBall.colour = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	// Set position properties
	cueBall.curPos = vec3(20.0f, 1.0f, 0);
	cueBall.prevPos = cueBall.curPos;
	// Translate mesh to position
	cueBall.mesh.get_transform().translate(cueBall.curPos);
	// Add to  list of balls
	balls["cueBall"] = cueBall;

	ball ball1;
	ball1.name = "ball1";
	ball1.mesh = mesh(geometry_builder::create_sphere(50, 50));
	ball1.colour = vec4(1.0f, 0, 0, 1.0f);
	ball1.curPos = vec3(-30.0f, 1.0f, 0);
	ball1.prevPos = ball1.curPos;
	ball1.mesh.get_transform().translate(ball1.curPos);
	balls["ball1"] = ball1;

	ball ball2;
	ball2.name = "Ball 2";
	ball2.mesh = mesh(geometry_builder::create_sphere(50, 50));
	ball2.colour = vec4(1.0f, 0, 0, 1.0f);
	ball2.curPos = vec3(-30.0f, 1.0f, 5.0f);
	ball2.prevPos = ball2.curPos;
	ball2.mesh.get_transform().translate(ball2.curPos);
	balls["ball2"] = ball2;

	ball ball3;
	ball3.name = "Ball 3";
	ball3.mesh = mesh(geometry_builder::create_sphere(50, 50));
	ball3.colour = vec4(1.0f, 0, 0, 1.0f);
	ball3.curPos = vec3(-30.0f, 1.0f, -5.0f);
	ball3.prevPos = ball3.curPos;
	ball3.mesh.get_transform().translate(ball3.curPos);
	balls["ball3"] = ball3;

	ball ball4;
	ball4.name = "Ball 4";
	ball4.mesh = mesh(geometry_builder::create_sphere(50, 50));
	ball4.colour = vec4(1.0f, 0, 0, 1.0f);
	ball4.curPos = vec3(-25.0f, 1.0f, -2.5f);
	ball4.prevPos = ball4.curPos;
	ball4.mesh.get_transform().translate(ball4.curPos);
	balls["ball4"] = ball4;

	ball ball5;
	ball5.name = "Ball 5";
	ball5.mesh = mesh(geometry_builder::create_sphere(50, 50));
	ball5.colour = vec4(1.0f, 0, 0, 1.0f);
	ball5.curPos = vec3(-25.0f, 1.0f, 2.5f);
	ball5.prevPos = ball5.curPos;
	ball5.mesh.get_transform().translate(ball5.curPos);
	balls["ball5"] = ball5;

	ball ball6;
	ball6.name = "Ball 6";
	ball6.mesh = mesh(geometry_builder::create_sphere(50, 50));
	ball6.colour = vec4(1.0f, 0, 0, 1.0f);
	ball6.curPos = vec3(-20.0f, 1.0f, 0);
	ball6.prevPos = ball6.curPos;
	ball6.mesh.get_transform().translate(ball6.curPos);
	balls["ball6"] = ball6;
	
	// Table 
	tablePiece tabletop;
	// Create plane geom
	tabletop.mesh = mesh(geometry_builder::create_plane(80, 40));
	// Set position
	tabletop.pos = vec3(0, 0, 0);
	// Translate to pos
	tabletop.mesh.get_transform().translate(tabletop.pos);
	// Add to table pieces list
	table["tabletop"] = tabletop;

	mesh railing = mesh(geometry_builder::create_box(vec3(35.0f, 5.0f, 2.0f)));

	tablePiece rail1;
	rail1.mesh = railing;
	rail1.pos = vec3(20.0f, 0, 21.0f);
	rail1.mesh.get_transform().translate(rail1.pos);
	table["rail1"] = rail1;

	tablePiece rail2;
	rail2.mesh = railing;
	rail2.pos = vec3(-20.f, 0, 21.0f);
	rail2.mesh.get_transform().translate(rail2.pos);
	table["rail2"] = rail2;
	
	tablePiece rail3;
	rail3.mesh = railing;
	rail3.pos = vec3(20.0f, 0, -21.0f);
	rail3.mesh.get_transform().translate(rail3.pos);
	table["rail3"] = rail3;

	tablePiece rail4;
	rail4.mesh = railing;
	rail4.pos = vec3(-20.0f, 0, -21.0f);
	rail4.mesh.get_transform().translate(rail4.pos);
	table["rail4"] = rail4;

	tablePiece rail5;
	rail5.mesh = mesh(geometry_builder::create_box(vec3(2.0f, 5.0f, 35.0f)));
	rail5.pos = vec3(41.0f, 0, 0);
	rail5.mesh.get_transform().translate(rail5.pos);
	table["rail5"] = rail5;

	tablePiece rail6;
	rail6.mesh = rail5.mesh;
	rail6.pos = vec3(-82.0f, 0, 0);
	rail6.mesh.get_transform().translate(rail6.pos);
	table["rail6"] = rail6;

	// Set up lines for spacial partitioning grid
	gridLineX.add_buffer(vector<vec3>{ vec3(-40.0f, 2.0f, 0), vec3(40.0f, 2.0f, 0) }, BUFFER_INDEXES::POSITION_BUFFER);
	gridLineX.add_buffer(vector<vec4>{ vec4(1.0, 1.0, 1.0, 1.0), vec4(1.0, 1.0, 1.0, 1.0) }, BUFFER_INDEXES::COLOUR_BUFFER);
	gridLineX.set_type(GL_LINES);
	lines["gridLineX"] = gridLineX;

	gridLineZ1.add_buffer(vector<vec3>{ vec3(-20.0f, 2.0f, -20.f), vec3(-20.0f, 2.0f, 20.0f) }, BUFFER_INDEXES::POSITION_BUFFER);
	gridLineZ1.add_buffer(vector<vec4>{ vec4(1.0, 1.0, 1.0, 1.0), vec4(1.0, 1.0, 1.0, 1.0) }, BUFFER_INDEXES::COLOUR_BUFFER);
	gridLineZ1.set_type(GL_LINES);
	lines["gridLineZ1"] = gridLineZ1;

	gridLineZ2.add_buffer(vector<vec3>{ vec3(0, 2.0f, -20.0f), vec3(0, 2.0f, 20.0f) }, BUFFER_INDEXES::POSITION_BUFFER);
	gridLineZ2.add_buffer(vector<vec4>{ vec4(1.0, 1.0, 1.0, 1.0), vec4(1.0, 1.0, 1.0, 1.0) }, BUFFER_INDEXES::COLOUR_BUFFER);
	gridLineZ2.set_type(GL_LINES);
	lines["gridLineZ2"] = gridLineZ2;
	
	gridLineZ3.add_buffer(vector<vec3>{ vec3(20.0f, 2.0f, -20.0f), vec3(20.0f, 2.0f, 20.0f) }, BUFFER_INDEXES::POSITION_BUFFER);
	gridLineZ3.add_buffer(vector<vec4>{ vec4(1.0, 1.0, 1.0, 1.0), vec4(1.0, 1.0, 1.0, 1.0) }, BUFFER_INDEXES::COLOUR_BUFFER);
	gridLineZ3.set_type(GL_LINES);
	lines["gridLineZ3"] = gridLineZ3;

	trajectory.add_buffer(vector<vec3>{ vec3(camPos.x, 1.0f, camPos.z), cueballPos }, BUFFER_INDEXES::POSITION_BUFFER);
	trajectory.add_buffer(vector<vec4>{ vec4(1.0, 1.0, 1.0, 1.0), vec4(1.0, 1.0, 1.0, 1.0) }, BUFFER_INDEXES::COLOUR_BUFFER);
	gridLineZ3.set_type(GL_LINES);
	lines["traj"] = trajectory;
}

// Rotate camera
void RotateCam(const double delta_time, vec3 axis)
{
	// Reset rotation value
	float rot = 0.0f;
	// Increment rot value
	rot += 1.2f * delta_time;
	// Update rotated camera position
	cam.set_position(rotate(camPos, rot, axis));
}

// Move cam back/forth whilst facing target
void ZoomCam(bool dir)
{
	// Find cam target pos
	vec3 target = cam.get_target();
	// Find facing direction
	vec3 lookAt = normalize(target - camPos);
	// If true, zoom out
	if (dir)
		camPos += -lookAt;
	// If false, zoom in
	else
		camPos += lookAt;

	// Adjust camera pos
	cam.set_position(camPos);
}

// Camera controls
void CameraControls(const double delta_time)
{
	// Rotate camera around target
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
	{
		RotateCam(delta_time, vec3(0.0, 1.0f, 0.0));
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
	{
		RotateCam(delta_time, vec3(0.0, -1.0f, 0.0));
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
	{
		RotateCam(delta_time, vec3(1.0f, 0.0, 0.0));
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
	{
		RotateCam(delta_time, vec3(-1.0f, 0.0, 0.0));
	}

	// Zoom cam
	if (glfwGetMouseButton(renderer::get_window(), GLFW_MOUSE_BUTTON_4))
	{
		// True = zooming out
		ZoomCam(true);

	}
	if (glfwGetMouseButton(renderer::get_window(), GLFW_MOUSE_BUTTON_5))
	{
		// False = zoom in
		ZoomCam(false);
	}
}

// As part of spacial partitioning, incomplete and not included in final
// Here to show working
void AssignCells(ball &b)
{
	
		//
		//// If position is on top row, either cell 1 or 2
		//if (b.curPos.x > -40.0f && b.curPos.x <= -20.0f)
		//{   // Top left, cell 1
		//	if (b.curPos.z > -20.0f && b.curPos.z <= 0)
		//	{
		//		// Add ball to list
		//		cell1[b.name] = b;
		//		// Update cell property
		//		b.curCell = cell1;
		//	}
		//	// Top right, cell 2
		//	else if (b.curPos.z >= 0 && b.curPos.z < 20.0f)
		//	{
		//		cell2[b.name] = b;
		//		b.curCell = cell2;
		//	}
		//}
		//// If position is in second row, either cell 3 or 4
		//if (b.curPos.x >= -20.0f && b.curPos.x <= 0.0f)
		//{
		//	if (b.curPos.z > -20.0f && b.curPos.z <= 0)
		//	{
		//		cell3[b.name] = b;
		//		b.curCell = cell3;
		//	}

		//	else if (b.curPos.z >= 0 && b.curPos.z < 20.0f)
		//	{
		//		cell4[b.name] = b;
		//		b.curCell = cell4;
		//	}
		//}
		//// If position is in third row, either cell 5 or 6
		//if (b.curPos.x >= 0.0f && b.curPos.x <= 20.0f)
		//{
		//	if (b.curPos.z > -20.0f && b.curPos.z <= 0)
		//	{
		//		cell5[b.name] = b;
		//		b.curCell = cell5;
		//	}
		//	else if (b.curPos.z >= 0 && b.curPos.z < 20.0f)
		//	{
		//		cell6[b.name] = b;
		//		b.curCell = cell6;
		//	}
		//}
		//if (b.curPos.x >= 20.0f && b.curPos.x <= 40.0f)
		//{
		//	if (b.curPos.z > -20.0f && b.curPos.z <= 0)
		//	{
		//		cell7[b.name] = b;
		//		b.curCell = cell7;
		//	}
		//	else if (b.curPos.z >= 0 && b.curPos.z < 20.0f)
		//	{
		//		cell8[b.name] = b;
		//		b.curCell = cell8;
		//	}
		//}
	


}

// Detect collisions with railings
dvec3 DetectRailings(ball &b, dvec3 vel)
{

	// Check for collisions with railing first
	if (b.curPos.z >= 19.0f || b.curPos.z <= -19.0f)
	{
		// Only if ball is not going to enter  middle pocket
		if (b.curPos.x > -37.0f && b.curPos.x < -1.5f || b.curPos.x < 37.0f && b.curPos.x > 1.5f)
		{
			// Reverse direction in z axis
			vel.z *= -1;
		}
	}
	
	else if (b.curPos.x >= 39.0f || b.curPos.x <= -39.0f)
	{
		// Only if ball is not going to enter corner pocket
		if (b.curPos.z < 17.0f || b.curPos.z > -17.0f)
		{
			// Reverse direction in x axis
			vel.x *= -1;
		}
	}


	return vel;

}

// Detect collisions between balls
void DetectBalls(ball &b, dvec3 vel)
{

	// Loop through other potential colliders
	for (auto &a : balls)
	{
		auto c = &a.second;
		// Ensure ball doesn't collide with itself
		if (b.name != c->name)
		{
			// Find vector between two balls
			dvec3 difference = c->curPos - b.curPos;
			// Find magnitude of difference vector
			double distance = length(difference);
			// If the distance is less than 2 x radius, there is a collision
			if (distance <= 2)
			{
				// Find speed for second ball
				double speed = length(vel);
				speed *= 100;
				// Scale dir vector by incoming speed from other ball
				dvec3 newDir = dvec3(difference.x * speed, difference.y, difference.z * speed);
				// Add this force as impulse to other ball
				c->AddImpulse(newDir);

				// First ball should travel at right angle to collider dir
				dvec3 dir = cross(difference,dvec3(0,1.0f,0));
				// Then normalise for direction
				dir = normalize(dir);
				// Adjust for speed
				dir = dvec3(dir.x * speed, dir.y, dir.z * speed);
				// Apply	
				b.AddImpulse(dir);	
			}
		}
	}

	//return vel;
}


// Update positions accordingly
void UpdatePhysics(const double delta_time)
{

	// Update positions 
	// Loop through balls
	for (auto &a : balls)
	{
		auto b = &a.second;
		// Calculate velocity from current and previous positions
		b->vel = b->curPos - b->prevPos;
		// Set prev pos to cur pos
		b->prevPos = b->curPos;
		// Calculate total acceleration from all forces
		dvec3 friction = dvec3(0);
		// In case velocity is 0 (don't want NaNs!)
		if (length2(b->vel) != 0)
		{
			// Friction equation
			friction = b->coefFriction * (normalize(b->vel));
		}

		// Detect collision with railings
		b->vel = DetectRailings(*b, b->vel);
		// Detect collisions with other balls
		DetectBalls(*b, b->vel);

		// Update position 
		b->curPos += b->vel + (b->forces + friction) * pow(delta_time, 2);


		//// Make ball roll - rotation axis is cross between velocity and plane normal
		//dvec3 rotAxis = cross(b->vel, dvec3(0, 1.0f, 0));
		//// 
		//// Reset rotation value
		//float rot = 0.0f;
		//// Increment rot value
		//rot += 1.2f * delta_time;
		//b->mesh.get_transform().translate(rotate(b->curPos, rot, rotAxis));



		// Reset forces
		b->forces = dvec3(0);
		b->vel = dvec3(0);

		
		
	}

	
}

// Translate balls to match updated positions
void MoveBalls()
{
	// For every ball
	for (auto &a : balls)
	{
		auto b = &a.second;
		// Update its position to match pos property
		b->mesh.get_transform().position = b->curPos;
	}
}



bool update(double delta_time) {

	// Update camera
	camPos = cam.get_position();
	PV = cam.get_projection() * cam.get_view();
	cam.update(static_cast<float>(delta_time));
	renderer::setClearColour(0, 0, 0);
	// Listen for input camera controls
	CameraControls(delta_time);
	// Update cue ball position
	cueballPos = balls["cueBall"].curPos;

	// Cue ball strike
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_0))
	{
	
		// Work out direction camera is facing
		// This is the direction the ball will travel once struck
		dvec3 dir = (cam.get_target() - cam.get_position());
		// Don't want to chip balls into the air
		dir.y = 0.0f;
		// Get unit vector of direction
		dir = normalize(dir);

		// Work out stregth of shot
		// Default is 200.0f for testing
		float strength = 200.0f;
		// Scale direction unit vector by strength
		dir *= strength;
		// Work out how much force is applied via spring
		dvec3 newForce = ((k * t) / m) * dir;
		// Add this to forces
		balls["cueBall"].AddImpulse(newForce);
	}
	
	// Toggle spacial partitioning grid on screen
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_G))
	{
		showGrid = !showGrid;
		
	}

	
	UpdatePhysics(delta_time);
	MoveBalls();
	// Ensure cam always looks at cue ball
	cam.set_target(balls["cueBall"].curPos);

	return true;
}

bool load_content() {

	// Setup scene
	Init();

	// Create scene objects (table, balls etc)
	CreateSceneObjs();
	
	return true;
}

bool render() {

	auto V = cam.get_view();
	auto P = cam.get_projection();

	// Render all balls
	for (auto &e : balls)
	{
		// Grab second value (the object itself and not the string id)
		auto m = e.second;
		// Bind effect
		renderer::bind(effP);
		// Create MVP 
		auto M = m.mesh.get_transform().get_transform_matrix();
		auto MVP = P * V * M;
		mat3 N(1.0f);
		// Shader/lighting fx
		mat.set_diffuse(m.colour);
		renderer::bind(mat, "mat");
		renderer::bind(light, "light");
		glUniformMatrix4fv(effP.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
		glUniformMatrix4fv(effP.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));
		glUniformMatrix3fv(effP.get_uniform_location("N"), 1, GL_FALSE, value_ptr(N));
		renderer::render(m.mesh);
	}

	// Render all table pieces in table
	for (auto &e : table)
	{
		// Grab second value (the object itself and not the string id)
		auto m = e.second;
		// Bind effect
		renderer::bind(effP);
		// Create MVP 
		auto M = m.mesh.get_transform().get_transform_matrix();
		auto MVP = P * V * M;
		mat3 N(1.0f);
		// Shader/lighting fx
		mat.set_diffuse(m.colour);
		renderer::bind(mat, "mat");
		renderer::bind(light, "light");
		glUniformMatrix4fv(effP.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
		glUniformMatrix4fv(effP.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));
		glUniformMatrix3fv(effP.get_uniform_location("N"), 1, GL_FALSE, value_ptr(N));
		renderer::render(m.mesh);
	}

	// If grid has been toggled on, render all grid lines 
	if (showGrid)
	{
		for (auto &l : lines)
		{
			auto m = l.second;
			// Render line
			renderer::bind(effP);
			// Create MVP 
			auto M = mat4(1.0);
			auto MVP = M * PV;
			mat3 N(1.0f);
			// Shader/lighting fx
			mat.set_diffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));
			renderer::bind(mat, "mat");
			renderer::bind(light, "light");
			glUniformMatrix4fv(effP.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
			glUniformMatrix4fv(effP.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));
			glUniformMatrix3fv(effP.get_uniform_location("N"), 1, GL_FALSE, value_ptr(N));
			CHECK_GL_ERROR;
			renderer::render(m);
		}
	}


	return true;
}

void main() {
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}