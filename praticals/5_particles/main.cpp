#include "game.h"
#include "physics.h"
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <graphics_framework.h>
#include <phys_utils.h>
#include <thread>

using namespace std;
using namespace graphics_framework;
using namespace glm;
#define physics_tick 1.0 / 60.0

// List of Entity objects created
static vector<unique_ptr<Entity>> SceneList;
static unique_ptr<Entity> floorEnt;

// For adding particles 
unique_ptr<Entity> CreateParticle() {
  // Create a new Entity
  unique_ptr<Entity> ent(new Entity());
  // Pos is property of Entity struct. Set random z position
  ent->SetPosition(vec3(0, 5.0 + (double)(rand() % 200) / 20.0, 0));
  // Create a new physics component
  unique_ptr<Component> physComponent(new cPhysics());
  // Create render component to define & render shape
  unique_ptr<cShapeRenderer> renderComponent(new cShapeRenderer(cShapeRenderer::SPHERE));
  // Assign a random colour to render comp attached to Entity
  renderComponent->SetColour(phys::RandomColour());
  // Add created components
  ent->AddComponent(physComponent);
  ent->AddComponent(unique_ptr<Component>(new cSphereCollider()));
  ent->AddComponent(unique_ptr<Component>(move(renderComponent)));
  return ent;
}
 

// Tracks time passed
bool update(double delta_time) {
  static double t = 0.0;
  static double accumulator = 0.0;
  accumulator += delta_time;

  while (accumulator > physics_tick) {
	  // Checks and resolves collisions, updates positions
    UpdatePhysics(t, physics_tick);
    accumulator -= physics_tick;
    t += physics_tick;
  }

  for (auto &e : SceneList) {
    e->Update(delta_time);
  }


  // Move back and forward...
  if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
  {

	  // Set up a pointer of the physics compenent of the particle 
	  const auto particlePhysicsComponent = static_cast<cPhysics *>(SceneList[0]->GetComponents("Physics").at(0));
	  // Add impulse via that physics compenent
	  particlePhysicsComponent->AddImpulse(vec3(-100, 0, 0));
  }

  if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
  {
	  const auto particlePhysicsComponent = static_cast<cPhysics *>(SceneList[0]->GetComponents("Physics").at(0));
	  particlePhysicsComponent->AddImpulse(vec3(100, 0, 0));
  }


  phys::Update(delta_time);
  return true;
}

bool load_content() {
	// Initialise the physics!?
  phys::Init();

  // Add spherical particles to scene
  for (size_t i = 0; i < 2; i++) {
    SceneList.push_back(move(CreateParticle()));
  }
  floorEnt = unique_ptr<Entity>(new Entity());
  floorEnt->AddComponent(unique_ptr<Component>(new cPlaneCollider()));
   
  phys::SetCameraPos(vec3(20.0f, 10.0f, 20.0f));
  phys::SetCameraTarget(vec3(0, 10.0f, 0));
  InitPhysics();
  return true;
}

bool render() {
  for (auto &e : SceneList) {
    e->Render();
  }
  phys::DrawScene();
  return true;
}

void main() {
  // Create application
  app application;
  // Set load content, update and render methods
  application.set_load_content(load_content);
  application.set_update(update);
  application.set_render(render);
  // Run application
  application.run();
}